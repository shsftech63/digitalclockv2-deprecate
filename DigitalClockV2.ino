#include <Preferences.h>
Preferences storage;
#include <Arduino.h>
#include <U8g2lib.h>
#include <SPI.h>
#include <Wire.h>
#include <DHT.h>
#include <RTClib.h>

#define BACKLIGHT_PIN 3
#define DHTPIN 19
#define DHTTYPE DHT11

#define PIN_CLK 18
#define PIN_MOSI 23
#define PIN_RESET 4
#define PIN_CS 2

#define BTN1  35
#define BTN2 34
#define BTN3 39
#define LED_PIN 12

#define SELECTION_INDEX_DEPTH1 0
#define SELECTION_INDEX_DEPTH2 1
#define SELECTION_INDEX_DEPTH3 2
#define SELECTION_INDEX_DEPTH4 3

U8G2_HX1230_96X68_F_3W_SW_SPI u8g2(U8G2_R0, /* clock=*/ PIN_CLK, /* data=*/ PIN_MOSI, /* cs=*/ PIN_CS, /* reset=*/ PIN_RESET);

RTC_DS3231 rtc;
DHT dht(DHTPIN, DHTTYPE);

int btn1Pressed,
  btn2Pressed,
  btn3Pressed,
  currentMillis;
  
void initialize(void) {
  Serial.begin(115200); 
  initializePins();
  initializeDHT();
  initializeRTC();
  initializeU8g2();
  initializeStorage();
}

void initializePins(void) {
  pinMode(BTN1, INPUT);  
  pinMode(BTN2, INPUT);  
  pinMode(BTN3, INPUT);  
  pinMode(LED_PIN, OUTPUT);
}

void initializeDHT(void) {
  dht.begin();
}

void initializeRTC() {

  // dont forget to check RTClib.cpp.
  // Line 1347
  // i changed this to make it work on ESP32
  if (!rtc.begin()) {
    return;
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
}

void initializeU8g2() {
  u8g2.begin();
  u8g2.enableUTF8Print();
  u8g2.setDisplayRotation(U8G2_R0);
}

void initializeStorage(void) {
  const int TOTAL_ALARMS = 5;
  for (int i = 0 ; i < TOTAL_ALARMS; i ++) {
    char ns[10];
    sprintf(ns, "alarm%d", i);
    storage.begin(ns);
  }
}

char *tileMenuList[] = {
  "Time",
  "Date",
  "Alarm",
  "Notif.",
  "Exit",
};

char *menuList[] = {
  ">Time Sett.",
  ">Date Sett.",
  ">Alarm Sett.",
  ">Notif. Sett.",
  "<Exit",
};

uint8_t uiTileSelect(char *items[], int totalItems, int index, const uint8_t *font, uint8_t offsetTop = 0) {
  u8g2.setFont(font);
  u8g2.drawRFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight(), 0);
  
  if (index <= 0) {
    index = 0;
  }

  if (index >= totalItems) {
    index = totalItems - 1;
  }
  
  u8g2.drawStr(getTextCenterX(items[index]), offsetTop, items[index]);

  if (index  > 0 && index < totalItems) {
    u8g2.drawStr(2, offsetTop, "<");
  }

  if (index < totalItems - 1 ) {
    u8g2.drawStr(u8g2.getDisplayWidth() - 14, offsetTop, ">");    
  }
}

uint8_t uiListSelect(char *items[], int totalItems, int index, int rowsPerPage, const uint8_t *font, char *title) {  
  const int ROW_PER_PAGE = 4;
  if (!rowsPerPage) {
    rowsPerPage = ROW_PER_PAGE;
  }
  int boxOffset = 2,
      titleOffsetTop = 12,
      marginLeft = 4,
      marginRight = 4,
      fontHeight = 13,
      boxOffsetTop = 2,
      totalPage = ceil((double)totalItems / rowsPerPage),
      ctr = 1,
      st = ceil((double)index / rowsPerPage),
      stepper = 0;
      
  u8g2.setFont(font);
  u8g2.setFontMode(1);
  u8g2.drawRFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight(), 0);

  u8g2.drawStr(getTextCenterX(title), titleOffsetTop, title);
  
  if (st < rowsPerPage && index >= rowsPerPage) {
    stepper = (index - rowsPerPage) + 1;
  }

  int xItemPos = 15;
  
  for (int i = stepper; i < totalItems; i++) {
    u8g2.setDrawColor(2);
    u8g2.drawStr(marginLeft, (fontHeight * ctr) + titleOffsetTop, items[i]);
    if (i == index) {
      u8g2.drawBox(1, (fontHeight * (ctr - 1)) + boxOffsetTop + titleOffsetTop, u8g2.getDisplayWidth() - 2, fontHeight); 
    }
    ctr++;
  }
}

uint8_t uiTimeSelect(int hourValue, int minuteValue, int secondValue, int index = 1) {  
  char *title = "Set Time", timeStr[10];
  short int fontHeight = 14,
    timeStrOffsetTop = 10;
  u8g2.setFontMode(1);
  u8g2.setFont(u8g2_font_profont15_tf);
  u8g2.setDrawColor(2);
  u8g2.drawRFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight(), 0);
  u8g2.drawStr(getTextCenterX(title), 15, title);
  u8g2.drawBox(1, 0, u8g2.getDisplayWidth() - 2, 20); 
  u8g2.setFont(u8g2_font_profont17_tf);
  sprintf(timeStr, "%s:%s", printDigits(hourValue), printDigits(minuteValue));
  u8g2.drawStr(getTextCenterX(timeStr), getTextCenterY() + timeStrOffsetTop, timeStr);
  int boxWidth = u8g2.getStrWidth(timeStr) / 2;

  if (index >= 1) {
    index = 1;
  }

  if (index <= 0) {
    index = 0;
  }
  
  int xPos = ((u8g2.getDisplayWidth() / 2) - (u8g2.getStrWidth(timeStr) / 2) - 1) + ((boxWidth + 3) * index);
  u8g2.drawBox(xPos, getTextCenterY() - 5, boxWidth - 1, 20); 
}

uint8_t uiDateSelect(int monthValue, int dayValue, int yearValue, int index = 1, uint8_t offsetTop = 0) {  
  char *title = "Set Date", timeStr[15];
  short int fontHeight = 14,
    timeStrOffsetTop = 10;
  u8g2.setFontMode(1);
  u8g2.setFont(u8g2_font_profont15_tf);
  u8g2.setDrawColor(2);
  u8g2.drawRFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight(), 0);
  u8g2.drawStr(getTextCenterX(title), 15, title);
  u8g2.drawBox(1, 0, u8g2.getDisplayWidth() - 2, 20); 
  u8g2.setFont(u8g2_font_profont17_tf);
  sprintf(timeStr, "%s|%s|%s", printDigits(monthValue), printDigits(dayValue), printDigits(yearValue));
  u8g2.drawStr(getTextCenterX(timeStr), getTextCenterY() + timeStrOffsetTop, timeStr);
  int boxWidth = u8g2.getStrWidth(timeStr) / 3;
  
  if (index >= 2) {
    index = 2;
  }

  if (index <= 0) {
    index = 0;
  }
  
  int xPos = ((u8g2.getDisplayWidth() / 2) - (u8g2.getStrWidth(timeStr) / 2) - 5) + (boxWidth * index);
  // Adjust box width to cover all year section
  if (index == 2) {
    boxWidth = boxWidth + 8;
  } else {
    boxWidth = boxWidth - 6;
  }

  if (index == 0) {
    xPos = xPos + 2;
  }
  
  u8g2.drawBox(xPos, getTextCenterY() - 5, boxWidth, 20); 
}

uint8_t uiAlarmSelect(int monthValue, int dayValue, int yearValue, int hourValue, int minuteValue, int secondValue, int index = 1) {  
  char *title = "Alarm", timeStr[10];
  short int fontHeight = 14,
    timeStrOffsetTop = 10;
  u8g2.setFontMode(1);
  u8g2.setFont(u8g2_font_profont15_tf);
  u8g2.setDrawColor(2);
  u8g2.drawRFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight(), 0);
  u8g2.drawStr(getTextCenterX(title), 15, title);
  u8g2.drawBox(1, 0, u8g2.getDisplayWidth() - 2, 20); 
  u8g2.setFont(u8g2_font_profont17_tf);
//  sprintf(timeStr, "%s:%s", printDigits(hourValue), printDigits(minuteValue));
//  u8g2.drawStr(getTextCenterX(timeStr), getTextCenterY() + timeStrOffsetTop, timeStr);
//  int boxWidth = u8g2.getStrWidth(timeStr) / 2;
//
//  if (index >= 1) {
//    index = 1;
//  }
//
//  if (index <= 0) {
//    index = 0;
//  }
//  
//  int xPos = ((u8g2.getDisplayWidth() / 2) - (u8g2.getStrWidth(timeStr) / 2) - 1) + ((boxWidth + 3) * index);
//  u8g2.drawBox(xPos, getTextCenterY() - 5, boxWidth - 1, 20); 
}

uint8_t uiToggleSelect(bool enabled = false, uint8_t offsetTop = 0) {  
  const short int BOX_WIDTH = 50;
  int boxHeight = 20;
  int toggleBoxWidth = BOX_WIDTH;
  char *title = "On/Off";
  u8g2.setFontMode(1);
  u8g2.setFont(u8g2_font_profont15_tf);
  u8g2.setDrawColor(2);
  u8g2.drawRFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight(), 0);
  u8g2.drawStr(getTextCenterX(title), 15, title);
  u8g2.drawBox(1, 0, u8g2.getDisplayWidth() - 2, 20);
  if (!enabled) {
    toggleBoxWidth = BOX_WIDTH / 2;
  }
  u8g2.setDrawColor(1);
  u8g2.drawRFrame((u8g2.getDisplayWidth() / 2 ) - (BOX_WIDTH / 2), offsetTop, BOX_WIDTH, boxHeight, 3);
  u8g2.drawRBox((u8g2.getDisplayWidth() / 2 ) - (BOX_WIDTH / 2), offsetTop, toggleBoxWidth, boxHeight, 3); 
}

uint8_t uiNumberSelect(int value, uint8_t offsetTop = 0) {  
  char *title = "Adjust Value",
        valueStr[15];
  u8g2.setFontMode(1);
  u8g2.setFont(u8g2_font_profont15_tf);
  u8g2.setDrawColor(2);
  u8g2.drawRFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight(), 0);
  u8g2.drawStr(getTextCenterX(title), 15, title);
  u8g2.drawBox(1, 0, u8g2.getDisplayWidth() - 2, 20);
  
  u8g2.setFont(u8g2_font_profont22_tf);
  sprintf(valueStr, "%d", value);
  u8g2.drawStr(getTextCenterX(valueStr), getTextCenterY() + 15, valueStr);
}

uint8_t uiLoading(char *title) { 
  u8g2.setFont(u8g2_font_profont15_tf);
  u8g2.drawStr(getTextCenterX(title), getTextCenterY(), title);
}

int getTextCenterX(char *textValue) {
  return u8g2.getDisplayWidth() / 2 - (u8g2.getStrWidth(textValue) / 2);
}

int getTextCenterY() {
  return u8g2.getDisplayHeight() / 2;
}

int adjustValue(int value, int minLimit, int maxLimit) {
  if (value > maxLimit && maxLimit > 0) {
    value = maxLimit;
  }

  if (value < minLimit) {
    value = minLimit;
  }
  return value;
}

String printDigits(int num) {
  if (num <= 9) {
    return "0" + String(num);
  } 
  return String(num);
}

bool isBtn1Pressed() {
  if (digitalRead(BTN1) == HIGH) {
    btn1Pressed = true;
    return btn1Pressed;
  }
  return false;
}

bool isBtn2Pressed() {
  if (digitalRead(BTN2) == HIGH) {
    btn2Pressed = true;
    digitalWrite(LED_PIN, HIGH);
    return btn2Pressed;
  }
  digitalWrite(LED_PIN, LOW);
  return false;
}

bool isBtn3Pressed() {
  if (digitalRead(BTN3) == HIGH) {
    btn3Pressed = true;
    return btn3Pressed;
  }
  return false;
}

String getMeridiemLabel() {
  DateTime now = rtc.now();
  if (now.isPM()) {
    return "PM";
  }
  return "AM";
}

void initializeBtnStates() {  
  btn1Pressed = isBtn1Pressed();
  btn2Pressed = isBtn2Pressed();
  btn3Pressed = isBtn3Pressed();

  if (btn1Pressed || btn2Pressed || btn3Pressed) {
    delay(150);
  }
}

int selectionIndex[4] = {
  0, // Menu depth 1 
  0, // Menu depth 2
  0, // Menu depth 3 
  0, // Menu depth 4 
};

void setSelectionIndex(int index, int value) {
  selectionIndex[index] = value;  
}

int getSelectionIndex(int index) {
  return selectionIndex[index];
}

bool isSelectedIndex(int index, int value) {
  return selectionIndex[index] == value;
}

void render(void) {
  currentMillis = millis();
  initializeBtnStates();
  viewMenu();
  Serial.println(String(isBtn1Pressed()) + ":" + String(isBtn2Pressed()) + ":" + String(isBtn3Pressed()));
}

void viewDummy(void) {
}

void viewMenu() {
  char *items[] = {
    "",
    "Settings",
    "Dummy"
  };

  int totalItems = sizeof(items) / sizeof(items[0]);  
  int depth = getSelectionIndex(SELECTION_INDEX_DEPTH1);
  if (depth <= 0) {
    setSelectionIndex(SELECTION_INDEX_DEPTH1, 0);
  }

  if (depth >= 2) {
    setSelectionIndex(SELECTION_INDEX_DEPTH1, 2);    
  }
  
  if (getSelectionIndex(SELECTION_INDEX_DEPTH2) > 0) {
    viewSubMenu();
    return;
  }
  
  if (depth == 0) {
    viewHome();
  } else {
     uiTileSelect(items, sizeof(items) / sizeof(items[0]), depth, u8g2_font_profont15_tf, 34);
  }
  
  if (btn1Pressed) {
    setSelectionIndex(SELECTION_INDEX_DEPTH1, depth - 1);
  }

  if (btn2Pressed && depth > 0) {
    setSelectionIndex(SELECTION_INDEX_DEPTH2, 1);    
  }

  if (btn3Pressed) {
    setSelectionIndex(SELECTION_INDEX_DEPTH1, depth + 1);    
  }
}

void viewSubMenu() {
  int depth = getSelectionIndex(SELECTION_INDEX_DEPTH1);
  if (depth == 1) {
    viewSettings();
    return;
  }

  if (depth == 2) {
    viewDummy();
  }
}

void viewSettings() {  
  const int ROWS_PER_PAGE = 4;
  char *items[] = {
    "> Time",
    "> Date",
    "> Alarm",
    "> Notif.",
    "< Back" 
  };
  int totalItems = sizeof(items) / sizeof(items[0]);
  int depth2 = getSelectionIndex(SELECTION_INDEX_DEPTH2);
  int depth3 = getSelectionIndex(SELECTION_INDEX_DEPTH3);
  if (depth3 > 0) {
    viewSettingsSub();
    return;
  }
  uiListSelect(items, totalItems, depth2 - 1, ROWS_PER_PAGE, u8g2_font_profont15_tf, "Settings");

  if (btn1Pressed && depth2 > 1) {
    setSelectionIndex(SELECTION_INDEX_DEPTH2, depth2 - 1);   
  }

  if (btn2Pressed) {
    if (depth2 == totalItems) {
      setSelectionIndex(SELECTION_INDEX_DEPTH2, 0);       
    } else {
    setSelectionIndex(SELECTION_INDEX_DEPTH3, 1); 
    }
  }
  
  if (btn3Pressed && depth2 < totalItems) {
    setSelectionIndex(SELECTION_INDEX_DEPTH2, depth2 + 1);       
  }
}

void viewSettingsSub() {
  int depth = getSelectionIndex(SELECTION_INDEX_DEPTH2);
  switch(depth) {
    case 1:
      viewTimeSettings();
      break;
    case 2:
      viewDateSettings();
      break;
    case 3:
      viewAlarmSettings();
      break;
  }
}

int timeSet[3] = {
  0,
  0,
  0
};
int timeSetIndex = 0;
void viewTimeSettings(void) {
  uiTimeSelect(timeSet[0], timeSet[1], timeSet[2], timeSetIndex);
  int depth = getSelectionIndex(SELECTION_INDEX_DEPTH4);
  if (depth > 0) {
    adjustTimeSettings();
    return;
  }
  
  DateTime now = rtc.now();
  timeSet[0] = adjustValue(now.hour(), 0, 23);
  timeSet[1] = adjustValue(now.minute(), 0, 59);
  timeSet[2] = adjustValue(now.second(), 0, 59);
  
  if (btn1Pressed) {
      timeSetIndex = adjustValue(timeSetIndex - 1, -1, 2);
      if (timeSetIndex < 0) {
        setSelectionIndex(SELECTION_INDEX_DEPTH3, 0);
        timeSetIndex = 0;
      }
  }

  if (btn2Pressed) {
    setSelectionIndex(SELECTION_INDEX_DEPTH4, 1);
  }
  
  if (btn3Pressed) {
      timeSetIndex = adjustValue(timeSetIndex + 1, 0, 2);
  }
} 

void adjustTimeSettings(void) {
  if (btn1Pressed) {
    timeSet[timeSetIndex] = timeSet[timeSetIndex] - 1;
  }
    
  if (btn2Pressed) {
    DateTime now = rtc.now();
    rtc.adjust(DateTime(now.year(), now.month(), now.day(), timeSet[0], timeSet[1]));
    setSelectionIndex(SELECTION_INDEX_DEPTH4, 0);
  }
  
  if (btn3Pressed) {
    timeSet[timeSetIndex] = timeSet[timeSetIndex] + 1;
  }
}

int dateSet[3] = {
  1,
  1,
  1975
};
int dateSetIndex = 0;
void viewDateSettings(void) {
  int depth = getSelectionIndex(SELECTION_INDEX_DEPTH4);
  uiDateSelect(dateSet[0], dateSet[1], dateSet[2], dateSetIndex);
  if (depth > 0) {
    adjustDateSettings();
    return;
  }
  DateTime now = rtc.now();
  dateSet[0] = adjustValue(now.month(), 0, 12);
  dateSet[1] = adjustValue(now.day(), 0, 31);
  dateSet[2] = adjustValue(now.year(), 1975, 3000);
  
  if (btn1Pressed) {
      dateSetIndex = adjustValue(dateSetIndex - 1, -1, 2);
      if (dateSetIndex < 0) {
        setSelectionIndex(SELECTION_INDEX_DEPTH3, 0);
        dateSetIndex = 0;
      }
  }

  if (btn2Pressed) {
    setSelectionIndex(SELECTION_INDEX_DEPTH4, 1);
  }
    
  if (btn3Pressed) {
      dateSetIndex = adjustValue(dateSetIndex + 1, 0, 2);
  }
}


void adjustDateSettings(void) {
  if (btn1Pressed) {
    dateSet[dateSetIndex] = dateSet[dateSetIndex] - 1;
  }
    
  if (btn2Pressed) {
    DateTime now = rtc.now();
    rtc.adjust(DateTime(dateSet[2], dateSet[0], dateSet[1], now.hour(), now.minute()));
    setSelectionIndex(SELECTION_INDEX_DEPTH4, 0);
  }
  
  if (btn3Pressed) {
    dateSet[dateSetIndex] = dateSet[dateSetIndex] + 1;
  }
}


char *getAlarms() {
  const int TOTAL_ITEMS = 5;
  char *items[TOTAL_ITEMS];
  String alarm1 = storage.getString("alarm1");
  String alarm2 = storage.getString("alarm2");
  String alarm3 = storage.getString("alarm3");
  String alarm4 = storage.getString("alarm4");
  String alarm5 = storage.getString("alarm5");

  char *palarm1 = const_cast<char*>(alarm1.c_str());
  char *palarm2 = const_cast<char*>(alarm2.c_str());
  char *palarm3 = const_cast<char*>(alarm3.c_str());
  char *palarm4 = const_cast<char*>(alarm4.c_str());
  char *palarm5 = const_cast<char*>(alarm5.c_str());

  items[0] = palarm1;
  items[1] = palarm2;
  items[2] = palarm3;
  items[3] = palarm4;
  items[4] = palarm5;
  return items[0];  
}

void viewAlarmSettings(void) { 
  DateTime now  = rtc.now();
  const int TOTAL_ITEMS = 5;
  char *items[TOTAL_ITEMS];
  int depth3 = getSelectionIndex(SELECTION_INDEX_DEPTH3);
  int depth4 = getSelectionIndex(SELECTION_INDEX_DEPTH4);

  if (depth4 > 0) {
    adjustAlarm();
    return;
  }

  String alarm1 = storage.getString("alarm1");
  String alarm2 = storage.getString("alarm2");
  String alarm3 = storage.getString("alarm3");
  String alarm4 = storage.getString("alarm4");
  String alarm5 = storage.getString("alarm5");

  char *palarm1 = const_cast<char*>(alarm1.c_str());
  char *palarm2 = const_cast<char*>(alarm2.c_str());
  char *palarm3 = const_cast<char*>(alarm3.c_str());
  char *palarm4 = const_cast<char*>(alarm4.c_str());
  char *palarm5 = const_cast<char*>(alarm5.c_str());

  items[0] = palarm1;
  items[1] = palarm2;
  items[2] = palarm3;
  items[3] = palarm4;
  items[4] = palarm5;

//  for (int i = 0; i < 5; i++) {
//    char ns[10];
////    char v[30];
//    sprintf(ns, "alarm%d", i);
//    String value = storage.getString(ns);
////    char *v = const_cast<char*>(value.c_str());
//    char *v = strcpy(new char[value.length() + 1], value.c_str());
//
////    int len = sizeof(value);
////    char *v = new char[len + 1];
////    copy(value.begin(), value.end(), v);
////    v[len] = "\0";
//    Serial.println(v);
//    items[i] = v;
//    delete[] v;
//    
////    sprintf(ns, "alarm%d", i);
////    String s = storage.getString(ns);
//////    sprintf(v, "%c", s);
////    char *e = v;
////    items[i] = e;
////    Serial.println(items[i]);
//  } 
  
  uiListSelect(items, TOTAL_ITEMS, depth3 - 1, 4, u8g2_font_profont15_tf ,"ALARMS");
  if (btn1Pressed && depth3 > 0) {
    setSelectionIndex(SELECTION_INDEX_DEPTH3, depth3 - 1);
  }  
  
  if (btn2Pressed) {
//    char *a1 = "202014122330";
//    char *a2 = "202014122335";
//    char *a3 = "202012122335";
//    char *a4 = "202012122335";
//    char *a5 = "202012122132";
//    storage.putString("alarm1", a1);
//    storage.putString("alarm2", a2);
//    storage.putString("alarm3", a3);
//    storage.putString("alarm3", a4);
//    storage.putString("alarm4", a5);
    setSelectionIndex(SELECTION_INDEX_DEPTH4, 1);
  }

  if (btn3Pressed && depth3 < TOTAL_ITEMS-1) {
    setSelectionIndex(SELECTION_INDEX_DEPTH3, depth3 + 1);  
  }
}

void adjustAlarm() {
  
}

void viewHome(void) {
  DateTime now = rtc.now();
  char dateStr[20];
  sprintf (dateStr, "Tue., %s/%s/%d", printDigits(now.month()), printDigits(now.day()), now.year());
  u8g2.setFont(u8g2_font_profont12_tf);
  u8g2.drawStr(getTextCenterX(dateStr), 10, dateStr);  
 
  char timeStr[10];
  sprintf (timeStr, "%s:%s%s", printDigits(now.hour()), printDigits(now.minute()), getMeridiemLabel());
  u8g2.setFont(u8g2_font_profont22_tf);
  u8g2.drawStr(getTextCenterX(timeStr), getTextCenterY() + 7, timeStr);  

  char sensorStr[20];
  sprintf(sensorStr, "TEMP: %d HUM: %d", (int)trunc(dht.readTemperature()), (int)trunc(dht.readHumidity()));
  u8g2.setFont(u8g2_font_profont12_tf);
  u8g2.drawStr(getTextCenterX(sensorStr), u8g2.getDisplayHeight(), sensorStr);
}

void setup(void) {
  initialize();
}

short int item = 0;
void loop(void) {
  u8g2.clearBuffer();
  render();
  u8g2.sendBuffer();
}
